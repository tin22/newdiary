from django.db import models
from django.urls import reverse
from django.utils import timezone


class Entry(models.Model):
    """ Daily entry
    """
    timestamp = models.DateTimeField(verbose_name='Вреная отметка', default=timezone.now)
    record = models.TextField('Запись', blank=False, null=False)
    theme = models.ForeignKey('Theme', on_delete=models.DO_NOTHING,
                              related_name='entries',
                              blank=True, null=True
                              )

    class Meta:
        db_table = 'entries'
        ordering = ('-timestamp',)
        verbose_name_plural = 'Записи'

    def __str__(self):
        return f"{self.timestamp.strftime('%d.%m.%Y %H:%M')} {self.record}"

    def get_absolute_url(self):
        return reverse('entry_detail', args=[str(self.pk)])


class Theme(models.Model):
    """ Entry themes
    """
    name = models.CharField(max_length=100)
    relevant = models.BooleanField(default=True)
    note = models.TextField()

    class Meta:
        db_table = 'themes'
        verbose_name = 'Тема'
        verbose_name_plural = 'Темы'

    def __str__(self):
        return f"{self.name}"


