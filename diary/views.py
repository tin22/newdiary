from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render
from django.views.generic import TemplateView, ListView, DetailView
from django.views.generic.edit import CreateView
from diary.models import Entry, Theme


class HomepageView(TemplateView):
    template_name = 'diary/home.html'


class EntryListView(LoginRequiredMixin, ListView):
    queryset = Entry.objects.order_by('-timestamp')
    paginate_by = 10
    context_object_name = 'entry_list'
    template_name = 'diary/entry_list.html'


class EntryCreateView(LoginRequiredMixin, CreateView):
    model = Entry
    template_name = 'diary/entry_new.html'
    fields = ('timestamp', 'record', 'theme',)



class ThemeListView(LoginRequiredMixin, ListView):
    queryset = Theme.objects.all()
    context_object_name = 'theme_list'
    template_name = 'diary/theme_list.html'
