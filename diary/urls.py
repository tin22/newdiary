from django.urls import path, include

from . import views
# from .views import entry_search

# app_name = 'diary'

urlpatterns = [
    path('', views.HomepageView.as_view(), name='home'),
    # path('', views.EntryListView.as_view(), name='entry_list_all'),
    path('entry_list/', views.EntryListView.as_view(), name='entry_list_all'),
    path('entry/new/', views.EntryCreateView.as_view(), name='entry_new'),
    # path('entry/search/', entry_search, name='entry_search'),
    path('theme_list/', views.ThemeListView.as_view(), name='theme_list_all')
]
