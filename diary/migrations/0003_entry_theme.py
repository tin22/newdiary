# Generated by Django 4.1 on 2022-08-31 09:24

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('diary', '0002_alter_entry_options_alter_theme_options_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='entry',
            name='theme',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='entries', to='diary.theme'),
        ),
    ]
